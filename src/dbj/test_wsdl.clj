(ns dbj.test-wsdl
  (:require [clojure.tools.cli :refer [parse-opts]]
            [clojure.java.io :as io])
  (:gen-class)
  (:import java.net.URL
           java.security.KeyStore
           [javax.net.ssl HttpsURLConnection KeyManagerFactory SSLContext]
           se.vv.rdt.secure.ExternalWebServiceSecure))

(defn init-ssl-context-from-pkcs12
  "Initialize ssl context from a pkcs12 file.
  Note: You can use io.netty.handler.ssl.JdkSslContext to use this context with Netty."
  [cert-file passwd]
  (let [sc (SSLContext/getInstance "TLS")
        kmf (KeyManagerFactory/getInstance (KeyManagerFactory/getDefaultAlgorithm))
        ks (KeyStore/getInstance (KeyStore/getDefaultType))]
    (.load ks cert-file (.toCharArray passwd))
    (.init kmf ks (.toCharArray passwd))
    (.init sc (.getKeyManagers kmf) nil nil)
    sc))

(defn setup-https
  "Setup ssl to use customer certificate."
  [cert-file cert-pwd]
  (let [ssl-context (init-ssl-context-from-pkcs12 cert-file cert-pwd)]
    
    (HttpsURLConnection/setDefaultSSLSocketFactory (.getSocketFactory ssl-context))))

(defn make-secure-rdt-port
  [url]
  (-> (ExternalWebServiceSecure. (URL. url))
      (.getExternalWebServiceSecureSoap)))

(defn leverera-foreteelse [port code xml-file]
  (.levereraForeteelse port
                       code
                       (slurp xml-file)))

(defn ping [port]
  (.ping port))

(defn cert-info [port]
  (.getCertInfo port))

(def cli-options  
  [[nil "--wsdl wsdl-url" "URL to the wsdl file"]
   [nil "--cmd command" "Command (ping | cert-info | leverera)"]
   [nil "--cert-file filename" "Certificate file"]
   [nil "--cert-pwd password" "certificate password"]
   ["-h" "--help"]])

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (let [{{:keys [cmd] :as opts} :options
         :keys [summary arguments]} (parse-opts args cli-options)]
    (if (or (not cmd) (:help opts))
      (println summary "\n"
               "--------------\n"
               "Exempel: java -jar test-wsdl.jar --wsdl https://s3-eu-west-1.amazonaws.com/kp.panorama.common/RDT/ExternalWebServiceSecure.xml --cert-file /path/to/ltf-cert.p12 --cert-pwd xxxx --cmd leverera 0186 /tmp/rdt-request.xml")
      (do
        (when (:cert-file opts)
          (setup-https (io/input-stream (:cert-file opts))
                       (:cert-pwd opts)))
        (let [port (make-secure-rdt-port (:wsdl opts))]
          (cond
            (= cmd "ping")
            (println (ping port))
            (= cmd "cert-info")
            (println (cert-info port))
            (= cmd "leverera")
            (println (apply (partial leverera-foreteelse port) arguments))))))))




