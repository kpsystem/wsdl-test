
package se.vv.rdt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for anonymous complex type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="HamtaTransformeradeKoordinaterSweRef99TMResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "hamtaTransformeradeKoordinaterSweRef99TMResult"
})
@XmlRootElement(name = "HamtaTransformeradeKoordinaterSweRef99TMResponse")
public class HamtaTransformeradeKoordinaterSweRef99TMResponse {

    @XmlElement(name = "HamtaTransformeradeKoordinaterSweRef99TMResult")
    protected String hamtaTransformeradeKoordinaterSweRef99TMResult;

    /**
     * Gets the value of the hamtaTransformeradeKoordinaterSweRef99TMResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHamtaTransformeradeKoordinaterSweRef99TMResult() {
        return hamtaTransformeradeKoordinaterSweRef99TMResult;
    }

    /**
     * Sets the value of the hamtaTransformeradeKoordinaterSweRef99TMResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHamtaTransformeradeKoordinaterSweRef99TMResult(String value) {
        this.hamtaTransformeradeKoordinaterSweRef99TMResult = value;
    }

}
