
package se.vv.rdt.secure;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for AF36Beslutsmyndighet.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * &lt;pre&gt;
 * &amp;lt;simpleType name="AF36Beslutsmyndighet"&amp;gt;
 *   &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&amp;gt;
 *     &amp;lt;enumeration value="Lansstyrelsen"/&amp;gt;
 *     &amp;lt;enumeration value="Transportstyrelsen"/&amp;gt;
 *     &amp;lt;enumeration value="Regering"/&amp;gt;
 *   &amp;lt;/restriction&amp;gt;
 * &amp;lt;/simpleType&amp;gt;
 * &lt;/pre&gt;
 * 
 */
@XmlType(name = "AF36Beslutsmyndighet")
@XmlEnum
public enum AF36Beslutsmyndighet {

    @XmlEnumValue("Lansstyrelsen")
    LANSSTYRELSEN("Lansstyrelsen"),
    @XmlEnumValue("Transportstyrelsen")
    TRANSPORTSTYRELSEN("Transportstyrelsen"),
    @XmlEnumValue("Regering")
    REGERING("Regering");
    private final String value;

    AF36Beslutsmyndighet(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AF36Beslutsmyndighet fromValue(String v) {
        for (AF36Beslutsmyndighet c: AF36Beslutsmyndighet.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
