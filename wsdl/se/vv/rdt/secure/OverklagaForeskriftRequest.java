
package se.vv.rdt.secure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * &lt;p&gt;Java class for OverklagaForeskriftRequest complex type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="OverklagaForeskriftRequest"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="ForeskriftId" type="{http://microsoft.com/wsdl/types/}guid"/&amp;gt;
 *         &amp;lt;element name="AterGallandeForeskrifter" type="{http://www.vv.se/rdt/secure/}ArrayOfGuid" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="OverKlagatDatum" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&amp;gt;
 *         &amp;lt;element name="BeslutsmydighetsKod" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="AndradKommentar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="OverklagadVia" type="{http://www.vv.se/rdt/secure/}AF36Beslutsmyndighet"/&amp;gt;
 *         &amp;lt;element name="Diarienummer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OverklagaForeskriftRequest", propOrder = {
    "foreskriftId",
    "aterGallandeForeskrifter",
    "overKlagatDatum",
    "beslutsmydighetsKod",
    "andradKommentar",
    "overklagadVia",
    "diarienummer"
})
public class OverklagaForeskriftRequest {

    @XmlElement(name = "ForeskriftId", required = true)
    protected String foreskriftId;
    @XmlElement(name = "AterGallandeForeskrifter")
    protected ArrayOfGuid aterGallandeForeskrifter;
    @XmlElement(name = "OverKlagatDatum", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar overKlagatDatum;
    @XmlElement(name = "BeslutsmydighetsKod")
    protected String beslutsmydighetsKod;
    @XmlElement(name = "AndradKommentar")
    protected String andradKommentar;
    @XmlElement(name = "OverklagadVia", required = true)
    @XmlSchemaType(name = "string")
    protected AF36Beslutsmyndighet overklagadVia;
    @XmlElement(name = "Diarienummer")
    protected String diarienummer;

    /**
     * Gets the value of the foreskriftId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeskriftId() {
        return foreskriftId;
    }

    /**
     * Sets the value of the foreskriftId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeskriftId(String value) {
        this.foreskriftId = value;
    }

    /**
     * Gets the value of the aterGallandeForeskrifter property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfGuid }
     *     
     */
    public ArrayOfGuid getAterGallandeForeskrifter() {
        return aterGallandeForeskrifter;
    }

    /**
     * Sets the value of the aterGallandeForeskrifter property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfGuid }
     *     
     */
    public void setAterGallandeForeskrifter(ArrayOfGuid value) {
        this.aterGallandeForeskrifter = value;
    }

    /**
     * Gets the value of the overKlagatDatum property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOverKlagatDatum() {
        return overKlagatDatum;
    }

    /**
     * Sets the value of the overKlagatDatum property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOverKlagatDatum(XMLGregorianCalendar value) {
        this.overKlagatDatum = value;
    }

    /**
     * Gets the value of the beslutsmydighetsKod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBeslutsmydighetsKod() {
        return beslutsmydighetsKod;
    }

    /**
     * Sets the value of the beslutsmydighetsKod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBeslutsmydighetsKod(String value) {
        this.beslutsmydighetsKod = value;
    }

    /**
     * Gets the value of the andradKommentar property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAndradKommentar() {
        return andradKommentar;
    }

    /**
     * Sets the value of the andradKommentar property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAndradKommentar(String value) {
        this.andradKommentar = value;
    }

    /**
     * Gets the value of the overklagadVia property.
     * 
     * @return
     *     possible object is
     *     {@link AF36Beslutsmyndighet }
     *     
     */
    public AF36Beslutsmyndighet getOverklagadVia() {
        return overklagadVia;
    }

    /**
     * Sets the value of the overklagadVia property.
     * 
     * @param value
     *     allowed object is
     *     {@link AF36Beslutsmyndighet }
     *     
     */
    public void setOverklagadVia(AF36Beslutsmyndighet value) {
        this.overklagadVia = value;
    }

    /**
     * Gets the value of the diarienummer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiarienummer() {
        return diarienummer;
    }

    /**
     * Sets the value of the diarienummer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiarienummer(String value) {
        this.diarienummer = value;
    }

}
