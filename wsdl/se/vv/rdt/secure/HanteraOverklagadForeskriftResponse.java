
package se.vv.rdt.secure;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for anonymous complex type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="HanteraOverklagadForeskriftResult" type="{http://www.w3.org/2001/XMLSchema}boolean"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "hanteraOverklagadForeskriftResult"
})
@XmlRootElement(name = "HanteraOverklagadForeskriftResponse")
public class HanteraOverklagadForeskriftResponse {

    @XmlElement(name = "HanteraOverklagadForeskriftResult")
    protected boolean hanteraOverklagadForeskriftResult;

    /**
     * Gets the value of the hanteraOverklagadForeskriftResult property.
     * 
     */
    public boolean isHanteraOverklagadForeskriftResult() {
        return hanteraOverklagadForeskriftResult;
    }

    /**
     * Sets the value of the hanteraOverklagadForeskriftResult property.
     * 
     */
    public void setHanteraOverklagadForeskriftResult(boolean value) {
        this.hanteraOverklagadForeskriftResult = value;
    }

}
