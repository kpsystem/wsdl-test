
package se.vv.rdt.secure;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the se.vv.rdt.secure package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _String_QNAME = new QName("http://www.vv.se/rdt/secure/", "string");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: se.vv.rdt.secure
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LevereraForeteelse }
     * 
     */
    public LevereraForeteelse createLevereraForeteelse() {
        return new LevereraForeteelse();
    }

    /**
     * Create an instance of {@link LevereraForeteelseResponse }
     * 
     */
    public LevereraForeteelseResponse createLevereraForeteelseResponse() {
        return new LevereraForeteelseResponse();
    }

    /**
     * Create an instance of {@link Ping }
     * 
     */
    public Ping createPing() {
        return new Ping();
    }

    /**
     * Create an instance of {@link PingResponse }
     * 
     */
    public PingResponse createPingResponse() {
        return new PingResponse();
    }

    /**
     * Create an instance of {@link GetCertInfo }
     * 
     */
    public GetCertInfo createGetCertInfo() {
        return new GetCertInfo();
    }

    /**
     * Create an instance of {@link GetCertInfoResponse }
     * 
     */
    public GetCertInfoResponse createGetCertInfoResponse() {
        return new GetCertInfoResponse();
    }

    /**
     * Create an instance of {@link ArrayOfGuid }
     * 
     */
    public ArrayOfGuid createArrayOfGuid() {
        return new ArrayOfGuid();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.vv.se/rdt/secure/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

}
