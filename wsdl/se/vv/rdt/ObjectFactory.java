
package se.vv.rdt;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the se.vv.rdt package. 
 * &lt;p&gt;An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _String_QNAME = new QName("http://www.vv.se/rdt/", "string");
    private final static QName _ArrayOfString_QNAME = new QName("http://www.vv.se/rdt/", "ArrayOfString");
    private final static QName _ArrayOfVardeGataVagNr_QNAME = new QName("http://www.vv.se/rdt/", "ArrayOfVardeGataVagNr");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: se.vv.rdt
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link HamtaDataKatalog }
     * 
     */
    public HamtaDataKatalog createHamtaDataKatalog() {
        return new HamtaDataKatalog();
    }

    /**
     * Create an instance of {@link HamtaDataKatalogResponse }
     * 
     */
    public HamtaDataKatalogResponse createHamtaDataKatalogResponse() {
        return new HamtaDataKatalogResponse();
    }

    /**
     * Create an instance of {@link HamtaForeteelser }
     * 
     */
    public HamtaForeteelser createHamtaForeteelser() {
        return new HamtaForeteelser();
    }

    /**
     * Create an instance of {@link HamtaForeteelserResponse }
     * 
     */
    public HamtaForeteelserResponse createHamtaForeteelserResponse() {
        return new HamtaForeteelserResponse();
    }

    /**
     * Create an instance of {@link HamtaUrspung }
     * 
     */
    public HamtaUrspung createHamtaUrspung() {
        return new HamtaUrspung();
    }

    /**
     * Create an instance of {@link HamtaUrspungResponse }
     * 
     */
    public HamtaUrspungResponse createHamtaUrspungResponse() {
        return new HamtaUrspungResponse();
    }

    /**
     * Create an instance of {@link HamtaFritext }
     * 
     */
    public HamtaFritext createHamtaFritext() {
        return new HamtaFritext();
    }

    /**
     * Create an instance of {@link HamtaFritextResponse }
     * 
     */
    public HamtaFritextResponse createHamtaFritextResponse() {
        return new HamtaFritextResponse();
    }

    /**
     * Create an instance of {@link ArrayOfString }
     * 
     */
    public ArrayOfString createArrayOfString() {
        return new ArrayOfString();
    }

    /**
     * Create an instance of {@link HamtaVagNamn }
     * 
     */
    public HamtaVagNamn createHamtaVagNamn() {
        return new HamtaVagNamn();
    }

    /**
     * Create an instance of {@link HamtaVagNamnResponse }
     * 
     */
    public HamtaVagNamnResponse createHamtaVagNamnResponse() {
        return new HamtaVagNamnResponse();
    }

    /**
     * Create an instance of {@link ArrayOfVardeGataVagNr }
     * 
     */
    public ArrayOfVardeGataVagNr createArrayOfVardeGataVagNr() {
        return new ArrayOfVardeGataVagNr();
    }

    /**
     * Create an instance of {@link HamtaVagNamnKarta }
     * 
     */
    public HamtaVagNamnKarta createHamtaVagNamnKarta() {
        return new HamtaVagNamnKarta();
    }

    /**
     * Create an instance of {@link HamtaVagNamnKartaResponse }
     * 
     */
    public HamtaVagNamnKartaResponse createHamtaVagNamnKartaResponse() {
        return new HamtaVagNamnKartaResponse();
    }

    /**
     * Create an instance of {@link HamtaTransformeradeKoordinaterSweRef99TM }
     * 
     */
    public HamtaTransformeradeKoordinaterSweRef99TM createHamtaTransformeradeKoordinaterSweRef99TM() {
        return new HamtaTransformeradeKoordinaterSweRef99TM();
    }

    /**
     * Create an instance of {@link HamtaTransformeradeKoordinaterSweRef99TMResponse }
     * 
     */
    public HamtaTransformeradeKoordinaterSweRef99TMResponse createHamtaTransformeradeKoordinaterSweRef99TMResponse() {
        return new HamtaTransformeradeKoordinaterSweRef99TMResponse();
    }

    /**
     * Create an instance of {@link HamtaDataKatalogVersion }
     * 
     */
    public HamtaDataKatalogVersion createHamtaDataKatalogVersion() {
        return new HamtaDataKatalogVersion();
    }

    /**
     * Create an instance of {@link HamtaDataKatalogVersionResponse }
     * 
     */
    public HamtaDataKatalogVersionResponse createHamtaDataKatalogVersionResponse() {
        return new HamtaDataKatalogVersionResponse();
    }

    /**
     * Create an instance of {@link VardeGataVagNr }
     * 
     */
    public VardeGataVagNr createVardeGataVagNr() {
        return new VardeGataVagNr();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.vv.se/rdt/", name = "string")
    public JAXBElement<String> createString(String value) {
        return new JAXBElement<String>(_String_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfString }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfString }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.vv.se/rdt/", name = "ArrayOfString")
    public JAXBElement<ArrayOfString> createArrayOfString(ArrayOfString value) {
        return new JAXBElement<ArrayOfString>(_ArrayOfString_QNAME, ArrayOfString.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfVardeGataVagNr }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ArrayOfVardeGataVagNr }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.vv.se/rdt/", name = "ArrayOfVardeGataVagNr")
    public JAXBElement<ArrayOfVardeGataVagNr> createArrayOfVardeGataVagNr(ArrayOfVardeGataVagNr value) {
        return new JAXBElement<ArrayOfVardeGataVagNr>(_ArrayOfVardeGataVagNr_QNAME, ArrayOfVardeGataVagNr.class, null, value);
    }

}
