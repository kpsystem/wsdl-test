
package se.vv.rdt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for anonymous complex type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="forskriftsid_Nyckel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "forskriftsidNyckel"
})
@XmlRootElement(name = "HamtaDataKatalogVersion")
public class HamtaDataKatalogVersion {

    @XmlElement(name = "forskriftsid_Nyckel")
    protected String forskriftsidNyckel;

    /**
     * Gets the value of the forskriftsidNyckel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForskriftsidNyckel() {
        return forskriftsidNyckel;
    }

    /**
     * Sets the value of the forskriftsidNyckel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForskriftsidNyckel(String value) {
        this.forskriftsidNyckel = value;
    }

}
