
package se.vv.rdt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for anonymous complex type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="HamtaVagNamnKartaResult" type="{http://www.vv.se/rdt/}ArrayOfVardeGataVagNr" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "hamtaVagNamnKartaResult"
})
@XmlRootElement(name = "HamtaVagNamnKartaResponse")
public class HamtaVagNamnKartaResponse {

    @XmlElement(name = "HamtaVagNamnKartaResult")
    protected ArrayOfVardeGataVagNr hamtaVagNamnKartaResult;

    /**
     * Gets the value of the hamtaVagNamnKartaResult property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfVardeGataVagNr }
     *     
     */
    public ArrayOfVardeGataVagNr getHamtaVagNamnKartaResult() {
        return hamtaVagNamnKartaResult;
    }

    /**
     * Sets the value of the hamtaVagNamnKartaResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfVardeGataVagNr }
     *     
     */
    public void setHamtaVagNamnKartaResult(ArrayOfVardeGataVagNr value) {
        this.hamtaVagNamnKartaResult = value;
    }

}
