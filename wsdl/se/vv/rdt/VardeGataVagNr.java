
package se.vv.rdt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for VardeGataVagNr complex type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="VardeGataVagNr"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="ID" type="{http://www.w3.org/2001/XMLSchema}int"/&amp;gt;
 *         &amp;lt;element name="VagNamnNr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="Kommun" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *         &amp;lt;element name="FriText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VardeGataVagNr", propOrder = {
    "id",
    "vagNamnNr",
    "kommun",
    "friText"
})
public class VardeGataVagNr {

    @XmlElement(name = "ID")
    protected int id;
    @XmlElement(name = "VagNamnNr")
    protected String vagNamnNr;
    @XmlElement(name = "Kommun")
    protected String kommun;
    @XmlElement(name = "FriText")
    protected String friText;

    /**
     * Gets the value of the id property.
     * 
     */
    public int getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     */
    public void setID(int value) {
        this.id = value;
    }

    /**
     * Gets the value of the vagNamnNr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVagNamnNr() {
        return vagNamnNr;
    }

    /**
     * Sets the value of the vagNamnNr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVagNamnNr(String value) {
        this.vagNamnNr = value;
    }

    /**
     * Gets the value of the kommun property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKommun() {
        return kommun;
    }

    /**
     * Sets the value of the kommun property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKommun(String value) {
        this.kommun = value;
    }

    /**
     * Gets the value of the friText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFriText() {
        return friText;
    }

    /**
     * Sets the value of the friText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFriText(String value) {
        this.friText = value;
    }

}
