
package se.vv.rdt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for anonymous complex type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="HamtaForeteelserResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/restriction&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "hamtaForeteelserResult"
})
@XmlRootElement(name = "HamtaForeteelserResponse")
public class HamtaForeteelserResponse {

    @XmlElement(name = "HamtaForeteelserResult")
    protected String hamtaForeteelserResult;

    /**
     * Gets the value of the hamtaForeteelserResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHamtaForeteelserResult() {
        return hamtaForeteelserResult;
    }

    /**
     * Sets the value of the hamtaForeteelserResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHamtaForeteelserResult(String value) {
        this.hamtaForeteelserResult = value;
    }

}
